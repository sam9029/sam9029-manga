
# SAM9029MANGA
# 前言
## 命名规范

`开头小写,双驼峰` 
`多字段 皆采用 下划线 链接`（css类名除外 使用中杠线链接）

- 文件名

> vue 文件
> page.vue
> pageName.vue

> js 文件
> page.js
> pageName.js

- 目录
> layout
> layoutInner

- css类名

~~~html
<element class='one header-cont header-inner-cont'></element>
~~~

- js变量
~~~js
// 变量
let one = null
let one_two = null
let one_two_three = null
// 全局变量 前缀gl
let gl_one = null

// 函数 "加 handle 前缀"
// 参数 "加 _ (下划线) 前缀"
function handleOne(_param,_query){}

function handleOneTwo(){}

~~~



## 设计资源
	- 图标 阿里iconfont
	

# C 端 Page Design

首页 `home`
分类页 `category`
漫画详情页 `manga_detail`
阅览漫画页 `read_page`

个人信息组件-浮动于页面（包含登录、注册等操作）  
	- 登录 `login`
	- 注册 `register`
	- 个人信息 `user_profile`
顶部组件  `nav_top`
	- 返回，去首页
	- 中部信息显示
底部组件  `footer`
	- 信息版权
搜索组件  `search`
logo 组件 `web_logo`

manga展示列表组件
	- 首页，分类，搜索结果使用
	- 动态 列数展示




# 目前任务
	- ！！！ 先写一端吧（H5端）
	- PC + H5 端
	- ✔实现 响应式 布局
		- (采用 JS 适应 方式)
		- postcss-pxtorem 插件 实现 自动 px转rem(停止使用)
	- ✔tabbar 的根据 页面尺寸的 动态隐藏显示
	- 顶部 tabBar 
	- 基础页面搭建
		首页
			- 首页推荐
			- 排行榜
		分类页
		漫画详情页
		阅览漫画页
		（PC）个人信息组件-浮动于页面
	
# Summary
##  阅览漫画页--代码设计
- 初始进入（是否知晓 页码总数）？（最好是API请求后返回本话总页数，目前测试，假设有一话10页）
- 懒加载 监听使用 IntersectionObserver 监听对象绑定在谁身上？
- 页数 浏览 监听

# Question
## 采用 rpx 还是 rem ?
	- rem，皆以750px为底稿尺寸
	- 因为 PC 大屏还是比较重要的，rpx 现在对于 大屏PC 好像不被推荐
	
## 使用uniapp时 H5有底部tabbar 但是PC Web 没有 如何设置？
	> - [uniapp-api-隐藏tabbar](https://uniapp.dcloud.net.cn/api/ui/tabbar.html#hidetabbar)
	> - [uniapp-web专题-宽屏适配指南](https://uniapp.dcloud.net.cn/tutorial/adapt.html#%E5%AE%BD%E5%B1%8F%E9%80%82%E9%85%8D%E6%8C%87%E5%8D%97)
	>	 - 提供了 一种 PC 首页 和 H5小屏首页 不是同一个页面来处理
	- 直接采用 JS 读取 客户端视窗大小控制 uni API hidetabbar 来实现

## （非首页，H5 小屏时）自定义顶部导航栏
	> [uniapp-自定义导航栏使用注意](https://uniapp.dcloud.net.cn/collocation/pages.html#customnav)

## （Bug）视图窗口在 980px 变换时，单位突变的问题
	- 项目技术：采用 uniapp, 使用 JS 函数来检测 视窗宽高 动态改变 html 根字体大小

## 忘记在 APP.vue 的style标签上`lang='scss'`导致在其中引入的`@import './assets/style/common.scss';`变量不可用

## (uview 使用问题) -- u-tabs 外部必须有定宽（或者就在最外部100%），否则外部盒子被撑开，无法左右滚动