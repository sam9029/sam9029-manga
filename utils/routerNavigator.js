export function goBack(){
	uni.navigateBack({
		delta: 1
	})
}
export function goHome(){
	uni.switchTab({
		url: '/pages/home/index'
	})
}
export function goMangaDetail(_mangaId) {
	uni.navigateTo({
		url: '/pages/manga_detail/index'
	})
}
export function goReadManga(_mangaId) {
	console.log('goReadManga');
	uni.navigateTo({
		url: '/pages/read_page/index'
	})
}