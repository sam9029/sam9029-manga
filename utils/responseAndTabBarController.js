/**
 * 作用：
 * 1.控制 tabBar 的隐藏与显示
 * - 逻辑：！！！页面尺寸 大于  768px 隐藏tabBar
 * 2.自适应 CSS 单位
 * 
 * 尺寸表（参考Edge开发者工具）：
 * 小设备 320px
 * 移动设备M 375px
 * 移动设备L 425px
 * 平板电脑 768px
 * 笔记本 1024px
 * 笔记本L 1440px
 */

// 底稿尺寸
let select_list = [,1290,1024,750]
const STANDARD_WEB_DESIGN_SIZE = select_list[2]
export default (function(doc, win) {
	// 获取全局文档节点，移动端屏幕是否翻转,单位自适应函数设定
	// JS Browser API orientationchange 为了获取移动端屏幕是否翻转（手机重力感应等引起屏幕长宽变化之类的）
	var docEl = doc.documentElement,
		resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
		recalc = function() {
			var clientWidth = docEl.clientWidth;
			if (!clientWidth) return;
	
			// 单位自适应
			if (clientWidth >= STANDARD_WEB_DESIGN_SIZE) {
				docEl.style.fontSize = '100px';
			} else {
				docEl.style.fontSize = 100 * (clientWidth / STANDARD_WEB_DESIGN_SIZE) + 'px';
			}
			
			// PC 768px 之后 控制 tabBar 隐藏与现实
			if(clientWidth >= 768) {
				uni.hideTabBar({fail:err=>{throw err}})
			}
			else {
				uni.showTabBar({fail:err=>{throw err}})
			}
				
			
		};

	if (!doc.addEventListener) return;

	// addEventListener 的三个参数 （事件名称，执行函数，事件的事件流过程触发的时期）
	win.addEventListener(resizeEvt, recalc, false);
	doc.addEventListener('DOMContentLoaded', recalc, false);
})(document, window)

