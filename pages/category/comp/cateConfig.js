// 元数据
// u-tabs 使用格式为 [{name:"XXX",}] 后面使用 函数进行处理
let tabData = {
	year: ['2023', '2022', '2021', '2020', '2019', '2018', '2017', '2016', '2015', '2014', '2013', '2012', '2011',
		'2010','2009','2008','2007','2006','2005',
	],
	country: ['日本', '国产', '英国', '美国', '韩国'],
	catetory: ['搞笑', '经典', '热血', '催泪', '治愈', '猎奇', '励志', '战斗', '后宫', '机战', '恋爱', '百合', '科幻', '奇幻', '推理', '校园',
		'运动', '魔法', '历史', '伪娘', '美少女', '萝莉', '亲子', '青春', '冒险', '竞技'
	],
}

// 作用看上面
export function initRenderTabData() {

	let renderTabData = {}

	for (let key in tabData) {
		let temp = []
		tabData[key].forEach(item => {
			temp.push({
				name: item
			})
		})
		renderTabData[key] = temp
	}

	return {
		...renderTabData
	}
}