import { defineConfig } from 'vite';
import uni from '@dcloudio/vite-plugin-uni';
// 动态 计算 px 转换为 rem
// import postCssPxToRem from 'postcss-pxtorem'
// import postcss_pxtorem from './postcss.config.js'

export default defineConfig({
	plugins: [uni()],
	// css: {
	// 	    postcss: {
	// 	      	plugins: [
	// 	        	postCssPxToRem({...postcss_pxtorem})
	// 	      	]
	// 	    }
	//     },
});